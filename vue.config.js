const { defineConfig } = require("@vue/cli-service");
module.exports = defineConfig({
  transpileDependencies: true,
  publicPath: "./",
  pluginOptions: {
    electronBuilder: {
      builderOptions: {
        productName: "AD-fish",
        //此处表示打包后的桌面应用程序的名字，可以是中文
      },
      nodeIntegration: true,
    },
  },
});
