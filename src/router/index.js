import { createRouter, createWebHashHistory } from "vue-router";
import HomeView from "../views/HomeShow.vue";
const routes = [
  {
    path: "/",
    name: "home",
    component: HomeView,
  },
  {
    path: "/deskTop",
    name: "deskTop",
    component: () => import("../views/DeskOpt.vue"),
  },
];

const router = createRouter({
  history: createWebHashHistory(),
  routes,
});

export default router;
