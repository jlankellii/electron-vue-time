# electron-vue-time

#### 介绍 📖

electron+vue3 实现下班倒计时

#### 描述 🐳

- 🐬 使用 vue cli 创建 vue 项目
- 🐬 使用 vue-cli-plugin-electron-builder 插件直接构建
- 🐬 使用 electron-icon-builder, 生成符合 Electron 的图标
- 🐬 基于 element-plus 库

#### 安装使用说明

1.  git clone https://gitee.com/jlankellii/electron-vue-time.git
2.  npm install
3.  本地启动：npm run electron:serve
4.  打包：npm run electron:build

#### 结果展示
![输入图片说明](%E5%80%92%E8%AE%A1%E6%97%B6.png)