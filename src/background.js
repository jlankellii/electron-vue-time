"use strict";

import {
  app,
  protocol,
  BrowserWindow,
  Tray,
  Menu,
  ipcMain,
  screen,
} from "electron";
import { createProtocol } from "vue-cli-plugin-electron-builder/lib";
import installExtension, { VUEJS3_DEVTOOLS } from "electron-devtools-installer";

const path = require("path");
const isDevelopment = process.env.NODE_ENV !== "production";

protocol.registerSchemesAsPrivileged([
  { scheme: "app", privileges: { secure: true, standard: true } },
]);

let win = null;
let winDesk = null;
async function createWindow() {
  // Create the browser window.
  win = new BrowserWindow({
    width: 220,
    height: 220,
    frame: false,
    fullscreen: false,
    transparent: true,
    backgroundColor: "#00000000",
    type: "toolbar", //创建的窗口类型为工具栏窗口
    hasShadow: false, //不显示阴影
    alwaysOnTop: true, //窗口是否总是显示在其他窗口之前\
    // eslint-disable-next-line no-undef
    icon: path.join(__static, "./icon.png"),
    webPreferences: {
      devTools: false,
      nodeIntegration: process.env.ELECTRON_NODE_INTEGRATION,
      contextIsolation: !process.env.ELECTRON_NODE_INTEGRATION,
    },
  });
  const { left, top } = {
    left: 20,
    top: screen.getPrimaryDisplay().workAreaSize.height - 300,
  };
  win.setPosition(left, top); //设置悬浮球位置
  win.once("ready-to-show", () => {
    win.show();
    // win.maximize(); // 最大化显示，先 show 再最大化，否则透明背景无效
  });

  win.setIgnoreMouseEvents(false);
  require("@electron/remote/main").enable(win.webContents);

  win.on("close", (e) => {
    e.preventDefault(); // 阻止退出程序
    win.setSkipTaskbar(true); // 取消任务栏显示
    win.hide(); // 隐藏主程序窗口
  });
  if (process.env.WEBPACK_DEV_SERVER_URL) {
    await win.loadURL(process.env.WEBPACK_DEV_SERVER_URL);
    if (!process.env.IS_TEST) win.webContents.openDevTools();
  } else {
    createProtocol("app");
    win.loadURL("app://./index.html");
  }
}

app.on("window-all-closed", () => {
  if (process.platform !== "darwin") {
    app.quit();
  }
});

app.on("activate", () => {
  if (BrowserWindow.getAllWindows().length === 0) createWindow();
});
// 创建桌面悬浮窗口
const winURL =
  process.env.NODE_ENV === "development"
    ? "http://localhost:8080"
    : "app://./index.html";

function createDeskWindow() {
  winDesk = new BrowserWindow({
    width: 300,
    height: 240,
    frame: false,
    resizable: false,
    type: "toobar",
    background: "#000",
    webPreferences: {
      nodeIntegration: true,
      contextIsolation: false,
    },
  });
  winDesk.loadURL(winURL + "/#/desktop");
  winDesk.setAlwaysOnTop(false); // 设置窗口置顶
  winDesk.on("closed", () => {
    winDesk = null;
  });
  require("@electron/remote/main").enable(winDesk.webContents);
}

app.on("ready", async () => {
  if (isDevelopment && !process.env.IS_TEST) {
    // Install Vue Devtools
    try {
      await installExtension(VUEJS3_DEVTOOLS);
    } catch (e) {
      console.error("Vue Devtools failed to install:", e.toString());
    }
  }
  createWindow();

  // 设置图标及菜单
  // eslint-disable-next-line no-undef
  const tray = new Tray(path.join(__static, "./icon.png")); // sets tray icon image
  const contextMenu = Menu.buildFromTemplate([
    // define menu items
    {
      label: "编辑",
      click: () => {
        createDeskWindow();
      },
    },
    {
      label: "退出",
      click: () => {
        win.destroy();
        app.quit();
      },
    },
  ]);
  tray.on("click", () => {
    if (!win) {
      createWindow();
    } else {
      win.show();
    }
  });
  tray.setContextMenu(contextMenu);
});

// Exit cleanly on request from parent process in development mode.
if (isDevelopment) {
  if (process.platform === "win32") {
    process.on("message", (data) => {
      if (data === "graceful-exit") {
        app.quit();
      }
    });
  } else {
    process.on("SIGTERM", () => {
      app.quit();
    });
  }
}

ipcMain.on("desktopClose", () => {
  setTimeout(() => {
    winDesk.hide();
  }, 500);
  win.destroy();
  createWindow();
});

ipcMain.on("desktopCancel", () => {
  winDesk.hide();
});
